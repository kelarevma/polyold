from django.contrib import admin


class DefaultModelAdmin(admin.ModelAdmin):
    """
    ModelAdmin характерезующая табличным представлением при получении списка
    """
    list_display = '__all__'
    list_per_page = 20

    def __init__(self, model, admin_site):
        if not self.list_display or self.list_display == '__all__':
            self.list_display = [field.name for field in model._meta.fields]

        if self.list_editable == '__all__':
            self.list_editable = [
                field.name for field in model._meta.fields if field.name in self.list_display
                                                              and field.editable and not field.auto_created and not field.primary_key
            ]
        if not self.list_display_links:
            self.list_display_links = [
                field_name for field_name in self.list_display
                if field_name not in self.list_editable
            ]
        if self.list_filter == '__all__':
            self.list_filter = [field.name for field in model._meta.fields]
        self.search_fields = [
            field.name for field in model._meta.fields if 'icontains' in field.get_lookups()
        ]
        super(DefaultModelAdmin, self).__init__(model, admin_site)


class EditReadOnlyAdmin(admin.ModelAdmin):
    """
    ModelAdmin закрывающая доступ для редактирования объектов из административной панели для всех пользователей
    """
    readonly_on_edit_fields = ()

    def has_add_permission(self, request):
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + self.readonly_on_edit_fields

        return self.readonly_fields
