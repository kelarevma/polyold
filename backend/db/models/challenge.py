from django.db import models

from .base import BaseModel
from .tag import Tag


class Challenge(BaseModel):
    """
    Модель задачи для решения
    """
    name = models.CharField(max_length=128, verbose_name="Название задачи")
    description = models.TextField(max_length=128, verbose_name="Описание задачи")

    score_points = models.IntegerField(default=5)
    tags = models.ManyToManyField(Tag, blank=True, verbose_name="Теги")

    class Meta:
        db_table = 'challenges'
        default_related_name = 'challenges'
        verbose_name_plural = "задачи"
        verbose_name = "задача"
