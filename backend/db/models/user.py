from django.contrib.auth.models import AbstractUser

from .base import BaseModel


class User(AbstractUser, BaseModel):
    """
    Модель пользователя
    """

    class Meta:
        db_table = 'users'
        default_related_name = 'users'
        verbose_name_plural = "пользователи"
        verbose_name = "пользователь"