from django.db import models

from db.models import BaseModel, Challenge, User


class Solution(BaseModel):
    """
    Модель решения
    """
    challenge = models.ForeignKey(Challenge, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    file = models.FileField(upload_to='./solutions')
    success = models.BooleanField(default=False)

    class Meta:
        default_related_name = 'solutions'
        db_table = 'solutions'
        verbose_name_plural = "решения"
        verbose_name = "решение"
