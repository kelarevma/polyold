from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from core.admin import DefaultModelAdmin, EditReadOnlyAdmin
from db import models


class ChallengeCaseAdminInline(admin.StackedInline):
    model = models.ChallengeCase


class ChallengeAdmin(DefaultModelAdmin):
    inlines = (ChallengeCaseAdminInline,)


class SolutionAdmin(EditReadOnlyAdmin):
    pass


class TagAdmin(DefaultModelAdmin):
    pass


admin.site.register(models.Challenge, ChallengeAdmin)
admin.site.register(models.User, UserAdmin)
admin.site.register(models.Solution, SolutionAdmin)
admin.site.register(models.Tag, TagAdmin)
