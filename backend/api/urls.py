from django.urls import include, path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from rest_framework import routers

from api.views.auth import RegisterView, CurrentUserView
from api.views.challenge import ChallengeViewSet
from api.views.solution import SolutionViewSet

router = routers.DefaultRouter()
router.register(r'challenges', ChallengeViewSet)
router.register(r'solutions', SolutionViewSet)

urlpatterns = [
    path('register/', RegisterView.as_view(), name='register_user'),
    path('current-user/', CurrentUserView.as_view(), name='current_user'),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
