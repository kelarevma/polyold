from rest_framework.fields import FileField
from rest_framework.serializers import ModelSerializer

from db.models import Solution


class SolutionSerializer(ModelSerializer):
    file = FileField()

    class Meta:
        model = Solution
        fields = '__all__'
        read_only_fields = ('success', 'user', 'created_on', 'updated_on')

