from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer
from db.models import Challenge


class ChallengeSerializer(ModelSerializer):
    tags = StringRelatedField(many=True)

    class Meta:
        model = Challenge
        fields = '__all__'
