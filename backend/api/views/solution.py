from rest_framework.mixins import CreateModelMixin
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ReadOnlyModelViewSet

from api.serializers.solution import SolutionSerializer
from db.models import Solution


class SolutionViewSet(ReadOnlyModelViewSet, CreateModelMixin):
    queryset = Solution.objects.all()
    serializer_class = SolutionSerializer

    permission_classes = [IsAuthenticated]
    filterset_fields = ('user', 'challenge')

    parser_classes = [FormParser, MultiPartParser, FileUploadParser]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
