from rest_framework import viewsets

from api.serializers.challenge import ChallengeSerializer
from db.models import Challenge


class ChallengeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Challenge.objects.all()
    serializer_class = ChallengeSerializer