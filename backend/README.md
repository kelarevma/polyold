# `./backend`
Корневая директория для backend приложения

## Инструкция по установке

- Установить `python` (Желательно версии 3.8.*)

- Октрыть терминал и перейти в папку `./backend`

- Cоздать виртаульное окружение `python -m venv venv`

- Активировать виртуальное окружение `venv\Sctipts\activate.bat`(Windows) `source ./venv/scripts/activate`

- Выполнить установку `pip install -r requirements.txt`

- Выполнить миграции `python manage.py migrate` (Пока настроена база данных sqllite)

- Создать локального пользователя `python manage.py createsuperuser` (Заполнить, пароль для удобства `qwerty1234`)

- Запустить проект `python manage.py runserver`

- Проверить, что все ок: зайти на http://127.0.0.1:8000/admin